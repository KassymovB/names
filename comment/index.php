<h2 class="flex items-center gap-4 font-bold">
    Комментарии <span class="text-gray-300">2</span>
</h2>

<form action="" class="mt-4 lg:w-2/3">

    <div class="flex flex-col w-80 space-y-3">

        <label for="name" class="text-sm text-trueGray-600">
            Имя
        </label>

        <input
                type="text"
                id="name"
                placeholder="Введите имя"
                class="px-3 py-2 border border-gray-300 rounded-md"
        >

    </div>

    <div class="flex flex-col mt-4 space-y-3">

        <label for="comment" class="text-sm text-trueGray-600">
            Ваш комментарий
        </label>

        <textarea
                name="comment"
                id="comment"
                cols="30"
                rows="10"
                placeholder="Введите текст"
                class="px-3 py-2 h-32 resize-none border border-gray-300 rounded-md"
        >
                </textarea>

    </div>

    <div class="flex justify-end mt-8">

        <?php include "information/inactive-btn.php"; ?>

    </div>

    <div class="mt-6 pt-6 border-t border-gray-300">

        <div class="space-y-3">

            <div class="flex items-center justify-between">
                <div class="flex items-center gap-1">
                    <img src="img/user.svg" alt="user">
                    <span class="text-sm font-bold">Дейенерис</span>
                </div>

                <div class="text-sm text-gray-300">
                    22.05.2021
                </div>

            </div>

            <p class="text-sm">
                спасибо за информацию! побежала собирать документы и стать матерью драконов
            </p>

        </div>

        <div class="mt-6 space-y-3">

            <div class="flex items-center justify-between">
                <div class="flex items-center gap-1">
                    <img src="img/user.svg" alt="user">
                    <span class="text-sm font-bold">Дейенерис</span>
                </div>

                <div class="text-sm text-gray-300">
                    22.05.2021
                </div>

            </div>

            <p class="text-sm">
                спасибо за информацию! побежала собирать документы и стать матерью драконов
            </p>

        </div>

        <div class="mt-6 space-y-3">

            <div class="flex items-center justify-between">
                <div class="flex items-center gap-1">
                    <img src="img/user.svg" alt="user">
                    <span class="text-sm font-bold">Дейенерис</span>
                </div>

                <div class="text-sm text-gray-300">
                    22.05.2021
                </div>

            </div>

            <p class="text-sm">
                спасибо за информацию! побежала собирать документы и стать матерью драконов
            </p>

        </div>

        <div class="mt-6 space-y-3">

            <div class="flex items-center justify-between">
                <div class="flex items-center gap-1">
                    <img src="img/user.svg" alt="user">
                    <span class="text-sm font-bold">Дейенерис</span>
                </div>

                <div class="text-sm text-gray-300">
                    22.05.2021
                </div>

            </div>

            <p class="text-sm">
                спасибо за информацию! побежала собирать документы и стать матерью драконов
            </p>

        </div>

        <div class="mt-6 space-y-3">

            <div class="flex items-center justify-between">
                <div class="flex items-center gap-1">
                    <img src="img/user.svg" alt="user">
                    <span class="text-sm font-bold">Дейенерис</span>
                </div>

                <div class="text-sm text-gray-300">
                    22.05.2021
                </div>

            </div>

            <p class="text-sm">
                спасибо за информацию! побежала собирать документы и стать матерью драконов
            </p>

        </div>

        <div
                x-show="openComment"
        >
            <div class="mt-6 space-y-3">

                <div class="flex items-center justify-between">
                    <div class="flex items-center gap-1">
                        <img src="img/user.svg" alt="user">
                        <span class="text-sm font-bold">Дейенерис</span>
                    </div>

                    <div class="text-sm text-gray-300">
                        22.05.2021
                    </div>

                </div>

                <p class="text-sm">
                    спасибо за информацию! побежала собирать документы и стать матерью драконов
                </p>

            </div>

            <div class="mt-6 space-y-3">

                <div class="flex items-center justify-between">
                    <div class="flex items-center gap-1">
                        <img src="img/user.svg" alt="user">
                        <span class="text-sm font-bold">Дейенерис</span>
                    </div>

                    <div class="text-sm text-gray-300">
                        22.05.2021
                    </div>

                </div>

                <p class="text-sm">
                    спасибо за информацию! побежала собирать документы и стать матерью драконов
                </p>

            </div>

            <div class="mt-6 space-y-3">

                <div class="flex items-center justify-between">
                    <div class="flex items-center gap-1">
                        <img src="img/user.svg" alt="user">
                        <span class="text-sm font-bold">Дейенерис</span>
                    </div>

                    <div class="text-sm text-gray-300">
                        22.05.2021
                    </div>

                </div>

                <p class="text-sm">
                    спасибо за информацию! побежала собирать документы и стать матерью драконов
                </p>

            </div>
        </div>

        <div class="flex justify-center mt-6">

            <button
                    @click.prevent="openComment = !openComment"
                    x-cloak=""
                    class="text-blue-900 font-bold focus:outline-none"
            >

                        <span
                                x-show="!openComment"
                                class="flex items-center"
                        >
                             Показать все

                            <svg class="w-6 h-6 fill-current">
                                <use href="#chevron-down"></use>
                            </svg>

                        </span>

                <span
                        x-show="openComment"
                        class="flex items-center"
                >
                             Скрыть

                            <svg class="w-6 h-6 fill-current">
                                <use href="#chevron-up"></use>
                            </svg>

                        </span>

            </button>

        </div>


    </div>

</form>
