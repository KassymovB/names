const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'current',
        lime: colors.lime,
        trueGray: colors.trueGray,

        black: {
          DEFAULT: '#1A1A1A',
        }
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['group-focus'],
      textColor: ['group-focus'],
    },
  },
  plugins: [],
}
