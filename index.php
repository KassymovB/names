<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Names</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="public/style.css">

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <style>
        [x-cloak] {display: none;}
    </style>

</head>

<body
        x-data="{openShare: false, openComment: false}"
        class="antialiased min-h-screen bg-gray-100"
>

    <div class="hidden">

        <?php include "src/icons.svg"; ?>

    </div>

    <?php include "header/header.php"; ?>


    <main class="relative">

        <?php include "search-result/index.php"; ?>

    </main>

    <?php include "footer/footer.php"?>

<script src="src/js/app.js"></script>
</body>
</html>