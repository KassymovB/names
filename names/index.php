<section class="pt-8 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <div class="text-sm text-trueGray-600">
            <span>Главная</span>
            /
            <span class="underline">Мужские имена</span>
        </div>

        <h1 class="mt-6 text-2xl text-blue-900 font-bold">Мужские имена</h1>

        <form action="" class="flex flex-wrap mt-6 space-y-6 sm:space-x-16">
            
            <div class="flex flex-col gap-y-2 w-80">

                <label for="1" class="text-trueGray-600">Происхождение</label>

                <select
                        name="name"
                        id="1"
                        class="px-2 py-3 border rounded-md focus:outline-none focus:border-blue-900 appearance-none"
                >

                    <option value="Все ">Все </option>
                    <option value="Все ">Все </option>
                    <option value="Все ">Все </option>
                    <option value="Все ">Все </option>
                </select>
            </div>

            <div class="flex flex-col">

                <label class="text-trueGray-600">Сортировать по</label>

                <div class="flex items-center mt-5 space-x-16">

                    <div class="flex items-center">
                        <input id="2" type="radio" class="w-4 h-4 cursor-pointer">
                        <label for="2">Алфавиту</label>
                    </div>

                    <div class="flex items-center">
                        <input id="3" type="radio" class="w-4 h-4 cursor-pointer">
                        <label for="3">Популярности</label>
                    </div>

                </div>

            </div>
        </form>

        <div class="mt-6 text-trueGray-600">
            Первая буква
        </div>

        <div class="overflow-auto">

            <div class="flex sm:justify-between gap-6 lg:gap-0 mt-2">
                <a href="#" class="text-blue-900 font-bold border-b-2 border-blue-900">Любая</a>
                <a href="#" class="font-bold">А</a>
                <a href="#" class="font-bold">Б</a>
                <a href="#" class="font-bold">В</a>
                <a href="#" class="font-bold">Г</a>
                <a href="#" class="font-bold">Д</a>
                <a href="#" class="font-bold">Е</a>
                <a href="#" class="font-bold">Ё</a>
                <a href="#" class="font-bold">Ж</a>
                <a href="#" class="font-bold">З</a>
                <a href="#" class="font-bold">И</a>
                <a href="#" class="font-bold">Й</a>
                <a href="#" class="font-bold">К</a>
                <a href="#" class="font-bold">Л</a>
                <a href="#" class="font-bold">М</a>
                <a href="#" class="font-bold">О</a>
                <a href="#" class="font-bold">П</a>
                <a href="#" class="font-bold">Р</a>
                <a href="#" class="font-bold">С</a>
                <a href="#" class="font-bold">Т</a>
                <a href="#" class="font-bold">У</a>
                <a href="#" class="font-bold">Ф</a>
                <a href="#" class="font-bold">Х</a>
                <a href="#" class="font-bold">Ц</a>
                <a href="#" class="font-bold">Ч</a>
                <a href="#" class="font-bold">Ш</a>
                <a href="#" class="font-bold">Э</a>
                <a href="#" class="font-bold">Ю</a>
                <a href="#" class="font-bold">Я</a>
            </div>

        </div>

        <div class="pt-7 mt-7 border-t-2 border-gray-300">

            <ul class="grid sm:grid-cols-4 grid-cols-2 gap-6 font-bold">

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

                <li>
                    <a href="#" class="hover:text-blue-900"> Аадитя</a>
                </li>

            </ul>

            <div class="flex items-center justify-end space-x-6 mt-16">

                <a href="#" class="text-blue-900 opacity-50">
                    <svg class="w-6 h-6 fill-current transform rotate-180">
                        <use href="#arrow-right"></use>
                    </svg>
                </a>

                <a href="#" class="flex items-center justify-center w-10 h-10 bg-blue-900 rounded-full text-white font-bold">
                    1
                </a>

                <a href="#" class="text-blue-900 font-bold opacity-50">
                    2
                </a>

                <a href="#" class="text-blue-900 font-bold opacity-50">
                    3
                </a>

                <a href="#" class="text-blue-900 font-bold opacity-50">
                    4
                </a>

                <a href="#" class="text-blue-900 font-bold opacity-50">
                    ...
                </a>

                <a href="#" class="text-blue-900 font-bold opacity-50">
                    16
                </a>

                <a href="#" class="text-blue-900">
                    <svg class="w-6 h-6 fill-current">
                        <use href="#arrow-right"></use>
                    </svg>
                </a>
            </div>

        </div>

    </div>

</section>


<section class="mt-14 bg-white">

    <div class="text-3xl">
        Реклама
    </div>

    <div class="flex justify-between px-4">

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

    </div>


</section>

