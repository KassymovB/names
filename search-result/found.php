<div class="mt-6 font-bold">
    Результаты по запросу “Али”
</div>

<div class="text-sm text-blue-900 opacity-50">
    Найдено 12
</div>

<ul class="mt-6 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>

<ul class="mt-6 pt-6 border-t border-gray-300 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>

<ul class="mt-6 pt-6 border-t border-gray-300 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>

<ul class="mt-6 pt-6 border-t border-gray-300 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>

<ul class="mt-6 pt-6 border-t border-gray-300 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>

<ul class="mt-6 pt-6 border-t border-gray-300 text-lg">

    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

    <li class="mt-2 text-gray-700">
        Происхождение: <span class="font-bold text-black">Арабское</span>
    </li>

    <li class="mt-2 text-gray-700">
        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
    </li>

    <li class="mt-2 text-gray-700">
        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
    </li>

    <li class="mt-4">

        <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
            Читать полностью
            <svg class="w-6 h-6 fill-current">
                <use href="#arrow-right"></use>
            </svg>
        </a>

    </li>

</ul>
