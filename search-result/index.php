<section class="py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <div class="text-sm text-trueGray-600">

            <span class="underline">Главная</span>
            /
            <span class="underline">Блог</span>

        </div>

        <div class="flex justify-between">

            <div class="lg:w-2/3">

              <?php include "not-found.php"?>

            </div>


            <div class="hidden lg:block w-1/4">

                <div class="flex items-center justify-center h-96 bg-trueGray-300 rounded-md">
                    Реклама
                </div>

            </div>

        </div>

    </div>

</section>

<section class="mt-14 bg-white">

    <div class="text-3xl">
        Реклама
    </div>

    <div class="flex justify-between px-4">

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

    </div>


</section>