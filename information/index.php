<section class="py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <div class="text-sm text-trueGray-600">

            <span class="underline">Главная</span>
            /
            <span class="underline">Мужские имена</span>
            /
            <span>Санжар</span>

        </div>

        <div class="flex justify-between">

            <div class="lg:w-2/3">

                <ul class="mt-6 text-lg">

                    <li class="text-2xl text-blue-900 font-bold">Санжар</li>

                    <li class="mt-2 text-trueGray-600">
                        Происхождение: <span class="font-bold text-black">Арабское</span>
                    </li>

                    <li class="mt-2 text-trueGray-600">
                        Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
                    </li>

                    <li class="mt-2 text-trueGray-600">
                        Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
                    </li>

                    <li class="mt-2 text-trueGray-600">
                        Похожие имена:
                            <a href="#" class="font-bold text-blue-900 border-b-2 border-blue-900">Сангар</a>,
                            <a href="#" class="font-bold text-blue-900 border-b-2 border-blue-900">Санджар</a>,
                            <a href="#" class="font-bold text-blue-900 border-b-2 border-blue-900">Зангар</a>,
                            <a href="#" class="font-bold text-blue-900 border-b-2 border-blue-900">Ханджар</a>
                    </li>

                </ul>

                <div class="flex items-center justify-between mt-5">

                    <div class="flex items-center space-x-6">

                        <button class="flex items-center space-x-1 focus:outline-none">

                            <svg class="w-7 h-7 fill-current text-white">
                                <use href="#comment"></use>
                            </svg>

                            <span class="text-gray-400">3</span>
                        </button>

                        <button class="flex items-center space-x-1 focus:outline-none">

                            <svg class="w-7 h-7 fill-current text-white">
                                <use href="#like"></use>
                            </svg>

                            <span class="text-gray-400">3</span>
                        </button>

                    </div>

                    <div class="relative">

                        <button
                                @click="openShare = !openShare"
                                class="flex items-center space-x-1 text-blue-900 font-bold text-sm focus:outline-none"
                        >

                            <span class="">Поделиться</span>

                            <svg class="w-5 h-5 fill-current ">
                                <use href="#share"></use>
                            </svg>

                        </button>


                        <ul
                                x-show="openShare"
                                x-cloak=""
                                x-transition:enter="transition-all duration-500 ease-liner"
                                x-transition:enter-start="opacity-0"
                                x-transition:enter-end="opacity-100"
                                x-transition:leave="transition-all duration-500 ease-liner"
                                x-transition:leave-start="opacity-100"
                                x-transition:enter-end="opacity-0"

                                class="absolute top-full right-0 mt-1 bg-white shadow-lg rounded-md text-sm overflow-hidden"
                        >

                            <li class="px-2 py-2 w-44 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/whatsApp.svg" alt="whatsApp">
                                     <span>WhatsApp</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/telegram.svg" alt="telegram">
                                     <span>Telegram</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/facebook.svg" alt="facebook">
                                     <span>Facebook</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/twitter.svg" alt="twitter">
                                     <span>Twitter</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/instagram.svg" alt="instagram">
                                     <span>Instagram</span>
                                </a>

                            </li>

                        </ul>

                    </div>

                </div>

                <p class="mt-3 pt-6 text-black leading-8 border-t border-gray-300">
                    Имя Санжар – мусульманское мужское имя. В переводе с древнетюркского языка означает «атакующий», «пронизывающий», «пронзающий», «острый» (как сабля, меч), поэтому также переводят как «сабля», «клинок» или же «кинжал». Существует иная трактовка – «крепость» в значении оборонительного сооружения. Варианты имени – Сангар, Санджар, Зангар, Ханджар. Имя активно употребляется не только среди казахов, турков и других потомков тюрков, но также и среди индусов. Мужчина по имени Санжар обладает уверенностью в себе и в собственных силах. Но не стоит судить лишь по тому, что видишь: Санжар, и правда, динамичен, находчив, решителен, но может оказаться и довольно грубым или резким. У него есть склонность быть авторитарным и властным, но также этот мужчина глубоко заботится о других. Его чувствительность может быть огромной, а его эмоции столь насыщенными, что делает Санжара столь же способным к великодушию, как и к ожесточенной ярости. Санжар хочет, чтобы его любили и восхищались им, но он никогда не забывает о влиянии, которое у него есть на других людей. Трудности стимулируют его, и он охотно борется с ними. Он умеет прекрасно аргументировать, и хотя он, по сути, индивидуалист, ему часто нужна помощь других людей, чтобы добиться успеха. Если он участвует в любых групповых мероприятиях, политических, спортивных или культурных, он захочет стать лидером.
                </p>

            </div>

            <div class="hidden lg:block w-1/4">

                <div class="flex items-center justify-center h-96 bg-trueGray-300">
                    Реклама
                </div>

                <div class="mt-8 py-6 px-3 border border-gray-400 rounded-md">

                    <h2 class="font-bold">Читайте также</h2>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>


                </div>

            </div>

        </div>

    </div>

</section>

<section class="mt-16 py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <h2 class="text-lg font-bold">Известные люди с именем Санжар</h2>
        
        <ul class="mt-4 pl-4 list-disc space-y-4">
            <li>
                Ахмад Санджар ((1084/1086-1157) полное имя - Муизз уд-Дин Абуль-Харис Ахмад Санджар ибн Малик-шах II;
                последний султан Сельджукской империи из династии Сельджукидов)
            </li>

            <li>
                Санжар Турсунов ((род.1986) узбекский футболист)
            </li>

            <li>
                Санжар Турсунов ((род.1986) узбекский футболист)
            </li>

            <li>
                Санжар Турсунов ((род.1986) узбекский футболист)
            </li>
        </ul>

    </div>

</section>

<section class="mt-16 py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

      <?php include "comment/index.php"; ?>

    </div>

</section>