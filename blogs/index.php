<section class="py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <div class="text-sm text-trueGray-600">

            <span class="underline">Главная</span>
            /
            <span class="underline">Блог</span>

        </div>

        <h1 class="mt-6 title">Блог</h1>

        <div class="grid sm:grid-cols-2 gap-6 mt-6">

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center text-gray-400">

                        Реклама

                        <img src="img/out.svg" alt="#" class="ml-2">

                    </div>


                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

        </div>

        <div class="flex items-center justify-end space-x-6 mt-16">

            <a href="#" class="text-blue-900 opacity-50">
                <svg class="w-6 h-6 fill-current transform rotate-180">
                    <use href="#arrow-right"></use>
                </svg>
            </a>

            <a href="#" class="flex items-center justify-center w-10 h-10 bg-blue-900 rounded-full text-white font-bold">
                1
            </a>

            <a href="#" class="text-blue-900 font-bold opacity-50">
                2
            </a>

            <a href="#" class="text-blue-900 font-bold opacity-50">
                3
            </a>

            <a href="#" class="text-blue-900">
                <svg class="w-6 h-6 fill-current">
                    <use href="#arrow-right"></use>
                </svg>
            </a>
        </div>



    </div>

</section>