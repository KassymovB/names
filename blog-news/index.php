<section class="py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <div class="text-sm text-trueGray-600">

            <span class="underline">Главная</span>
            /
            <span class="underline">Блог</span>
            /
            <span>Как поменять имя, фамилию или отчество</span>

        </div>

        <div class="flex justify-between">

            <div class="lg:w-2/3">

                <h1 class="title mt-4">Как поменять имя, фамилию или отчество</h1>
                
                <div class="mt-6 overflow-hidden rounded-md">
                    <img src="img/banana.png" alt="#" class="w-full h-full">
                </div>

                <div class="flex items-center justify-between mt-5">

                    <div class="flex items-center space-x-6">

                        <button class="flex items-center space-x-1 focus:outline-none">

                            <svg class="w-7 h-7 fill-current text-white">
                                <use href="#comment"></use>
                            </svg>

                            <span class="text-gray-400">3</span>
                        </button>

                        <button class="flex items-center space-x-1 focus:outline-none">

                            <svg class="w-7 h-7 fill-current text-white">
                                <use href="#like"></use>
                            </svg>

                            <span class="text-gray-400">3</span>
                        </button>

                    </div>

                    <div class="relative">

                        <button
                            @click="openShare = !openShare"
                            class="flex items-center space-x-1 text-blue-900 font-bold text-sm focus:outline-none"
                        >

                            <span class="">Поделиться</span>

                            <svg class="w-5 h-5 fill-current ">
                                <use href="#share"></use>
                            </svg>

                        </button>


                        <ul
                            x-show="openShare"
                            x-cloak=""
                            x-transition:enter="transition-all duration-500 ease-liner"
                            x-transition:enter-start="opacity-0"
                            x-transition:enter-end="opacity-100"
                            x-transition:leave="transition-all duration-500 ease-liner"
                            x-transition:leave-start="opacity-100"
                            x-transition:enter-end="opacity-0"

                            class="absolute top-full right-0 mt-1 bg-white shadow-lg rounded-md text-sm overflow-hidden"
                        >

                            <li class="px-2 py-2 w-44 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/whatsApp.svg" alt="whatsApp">
                                    <span>WhatsApp</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/telegram.svg" alt="telegram">
                                    <span>Telegram</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/facebook.svg" alt="facebook">
                                    <span>Facebook</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/twitter.svg" alt="twitter">
                                    <span>Twitter</span>
                                </a>

                            </li>

                            <li class="px-2 py-2 w-44 border-t border-gray-300 hover:bg-gray-100">

                                <a href="#" class="flex items-center space-x-3">
                                    <img src="img/instagram.svg" alt="instagram">
                                    <span>Instagram</span>
                                </a>

                            </li>

                        </ul>

                    </div>

                </div>

                <p class="mt-3 pt-6 text-black leading-8 border-t border-gray-300">

                    Надо только захотеть, а ещё оплатить госпошлину и собрать документы. Закон позволяет это сделать всем желающим старше 14 лет. Правда, детям до 18 лет потребуется согласие обоих родителей или других официальных представителей.

                </p>

                <dl class="mt-8">
                    <dt class="font-bold">
                        Какие документы нужны, чтобы поменять фамилию, имя или отчество
                    </dt>

                    <dd class="mt-2">
                        Понадобятся следующие бумаги:
                    </dd>
                </dl>
                
                <ul class="mt-2 pl-5 space-y-2 list-disc">
                    <li>
                        Заявление о перемене имени. Вероятнее всего, его заполнит при вас сотрудница загса, но вы можете сделать это заранее.
                    </li>

                    <li>
                        Паспорт гражданина РФ.
                    </li>

                    <li>
                        Свидетельство о рождении.
                    </li>

                    <li>
                        Квитанция об уплате госпошлины — по желанию. Эта информация должна быть доступна сотрудникам по внутренним каналам.
                    </li>

                    <li>
                        Свидетельство о заключении брака, если вы в браке.
                    </li>

                    <li>
                        Свидетельства о рождении детей, если у вас есть дети.
                    </li>

                    <li>
                        Свидетельство о расторжении брака, если вы разводились.
                    </li>

                    <li>
                        В заявлении вам придётся указать причину смены имени, и лучше выбирать не очень экзотическую. Например, напишите, что все вокруг вас называют этим, а не паспортным именем или что вы работаете под таким псевдонимом. Иначе вы рискуете столкнуться с препятствиями от работников загса.
                    </li>

                </ul>

            </div>

            <div class="hidden lg:block w-1/4">

                <div class="flex items-center justify-center h-96 bg-trueGray-300">
                    Реклама
                </div>

                <div class="mt-8 py-6 px-3 border border-gray-400 rounded-md">

                    <h2 class="font-bold">Читайте также</h2>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="flex gap-2 mt-6">

                        <a href="#" class="flex-shrink-0 w-24 h-16 overflow-hidden rounded-md">
                            <img src="img/interested.png" alt="#">
                        </a>

                        <a href="#" class="w-2/4">

                            <div class="text-sm font-bold whitespace-nowrap overflow-hidden overflow-ellipsis">
                                Названы самые популярные имена новорожденных в Казахстане
                            </div>

                            <div class="flex items-center gap-4 mt-1">
                                <div class="flex items-center text-gray-400">
                                    <img src="img/comment.svg" alt="#">
                                    1
                                </div>

                                <div class="flex items-center text-gray-400">
                                    <img src="img/favorite.svg" alt="#">
                                    3
                                </div>
                            </div>
                        </a>

                    </div>


                </div>

            </div>

        </div>

    </div>

</section>

<section class="mt-16 py-12 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">
        <?php include "comment/index.php"; ?>
    </div>

</section>