<header class="px-4 pt-4 pb-6 bg-gradient-to-t from-blue-900 to-blue-800">

    <div class="relative flex flex-col sm:flex-row sm:items-center
    sm:justify-between gap-6 sm:gap-16 sm:pr-32 lg:max-w-5xl lg:mx-auto h-full"
    >

        <div class="text-sm text-white font-bold sm:uppercase">
            <a href="#">name.com</a>
        </div>

        <div class="relative bg-white pl-4 pr-9 py-3 w-full rounded-lg">

            <input
                    placeholder="Имя или значение (например, мудрый)"
                    type="text"
                    name="search"
                    class="w-full h-full focus:outline-none text-sm sm:text-base"
            >

            <svg class="absolute top-2/4 right-2 transform -translate-y-2/4 w-7 h-7 text-blue-900 fill-current">
                <use href="#search"></use>
            </svg>

        </div>

        <div class="absolute sm:top-2/4 right-0 sm:transform sm:-translate-y-2/4 text-white font-semibold space-x-4">

            <a href="#" class="inline-flex items-center hover:underline uppercase opacity-80">
                Қаз
            </a>

            <a href="#" class="inline-flex items-center hover:underline uppercase underline">
                Рус
            </a>
        </div>

    </div>

</header>
