<div class="mt-5 flex items-center justify-center space-x-4">

    <div class="">
        <a href="#" class="flex items-center justify-center w-8 h-8 rounded-full bg-indigo-500 text-white">1</a>
    </div>

    <div class="">
        <a href="#" class="flex items-center justify-center w-8 h-8 rounded-full bg-indigo-500 text-white">2</a>
    </div>

    <div class="">
        <a href="#" class="flex items-center justify-center w-8 h-8 rounded-full bg-indigo-500 text-white">3</a>
    </div>

    <div class="">
        <a href="#" class="flex items-center justify-center w-8 h-8 rounded-full bg-indigo-500 text-white">...</a>
    </div>

    <div class="">
        <a href="#" class="flex items-center justify-center w-8 h-8 rounded-full bg-indigo-500 text-white">10</a>
    </div>

</div>
