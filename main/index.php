<section class="pb-10 bg-white">

    <div class="bg-blue-900">

        <div class="flex items-center mx-auto sm:px-10 lg:px-0 lg:max-w-5xl">

            <button class="w-full sm:w-auto sm:px-8 py-2 sm:rounded-tl-md rounded-tr-md bg-white text-sm sm:text-lg text-blue-900 font-bold focus:outline-none">
                Мужские имена
            </button>

            <button class="w-full sm:w-auto sm:px-8 py-1 rounded-tl-md rounded-tr-md text-sm sm:text-lg text-white font-bold opacity-80 focus:outline-none">
                Женские имена
            </button>

        </div>

    </div>

    <div class="mx-auto mt-8 px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <h1 class="inline-block px-3 py-1 bg-blue-300 font-bold rounded-md uppercase">
            Популярные имена
        </h1>

        <ul class="grid sm:grid-cols-2 gap-8 mt-6 pl-4 list-decimal text-lg font-bold">

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

            <li class="w-2/3 sm:w-full">

                <div>
                    <a href="#" class="flex items-center">
                        Санжар

                        <svg class="ml-3 w-6 h-6 fill-current text-blue-900">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>
                </div>

                <div class="pr-3 text-sm text-gray-600 font-medium whitespace-nowrap overflow-hidden overflow-ellipsis">
                    Арабское. В переводе с древнетюркского языка означает «атаку...
                </div>
            </li>

        </ul>

        <div class="flex justify-center mt-8">

            <a href="#" class="flex items-center px-3 py-1 bg-blue-900 rounded-2xl text-sm font-bold text-white">
                Все мужские имена

                <svg class="ml-2 w-6 h-6 fill-current">
                    <use href="#arrow-right"></use>
                </svg>
            </a>

        </div>

    </div>

</section>


<section class="mt-14 py-10 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <h1 class="inline-block px-3 py-1 bg-blue-300 font-bold rounded-md uppercase">
            интересное
        </h1>

        <div class="grid sm:grid-cols-2 gap-6 mt-6">

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center text-gray-400">

                        Реклама

                        <img src="img/out.svg" alt="#" class="ml-2">

                    </div>


                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

            <div class="flex gap-4">

                <a href="#" class="flex-shrink-0 overflow-hidden rounded-md">
                    <img src="img/interested.png" alt="#">
                </a>

                <a href="#">
                    <div class="text-sm sm:text-lg font-bold">
                        Названы самые популярные имена новорожденных в Казахстане
                    </div>

                    <div class="flex items-center gap-4 mt-1">
                        <div class="flex items-center text-gray-400">
                            <img src="img/comment.svg" alt="#">
                            1
                        </div>

                        <div class="flex items-center text-gray-400">
                            <img src="img/favorite.svg" alt="#">
                            3
                        </div>
                    </div>

                </a>
            </div>

        </div>

        <div class="flex justify-center mt-8">

            <a href="#" class="flex items-center px-3 py-1 bg-blue-900 rounded-2xl text-sm font-bold text-white hover:bg-blue-800">
                Все посты

                <svg class="ml-2 w-6 h-6 fill-current">
                    <use href="#arrow-right"></use>
                </svg>
            </a>

        </div>
    </div>

</section>


<section class="mt-14 py-10 bg-white">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <h1 class="inline-block px-3 py-1 bg-blue-300 font-bold rounded-md uppercase">
            имя дня
        </h1>


        <ul class="mt-6 text-lg">

                <li class="text-2xl text-blue-900 font-bold">Санжар</li>

                <li class="mt-2 text-gray-700">
                    Происхождение: <span class="font-bold text-black">Арабское</span>
                </li>

                <li class="mt-2 text-gray-700">
                    Похожие имена: <span class="font-bold text-black">Сангар, Санджар, Зангар, Ханджар</span>
                </li>

                <li class="mt-2 text-gray-700">
                    Значение: <span class="font-bold text-black">атакующий, пронзающий, острый (как сабля, меч)</span>
                </li>

                <li class="mt-4">

                    <a href="#" class="inline-flex items-center text-blue-900 font-bold text-sm">
                        Читать полностью
                        <svg class="w-6 h-6 fill-current">
                            <use href="#arrow-right"></use>
                        </svg>
                    </a>

                </li>

            </ul>

    </div>

</section>

<section class="mt-14 bg-white">

    <div class="text-3xl">
        Реклама
    </div>

    <div class="flex justify-between px-4">

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

        <div class="h-80 w-32 bg-gray-300"></div>

    </div>


</section>


