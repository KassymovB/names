<footer class="mt-14 py-6 bg-black">

    <div class="mx-auto px-5 sm:px-10 lg:px-0 lg:max-w-5xl">

        <ul class="flex flex-wrap items-center gap-6 sm:gap-12 w-full font-bold text-white">

            <li class="border-b-2 flex-shrink-0">Мужские имена</li>
            <li class="border-b-2 flex-shrink-0">Женские имена</li>
            <li class="border-b-2">Блог</li>
            <li class="sm:ml-auto">  © 2021 name.com</li>

        </ul>

    </div>

</footer>