<div
    x-show="openNav"
    @click="openNav = !openNav"
    x-transition:enter="transition ease-liner duration-400"
    x-transition:enter-start="opacity-0"
    x-transition:enter-end="opacity-100"
    x-transition:leave="transition ease-liner duration-400"
    x-transition:leave-start="opacity-100"
    x-transition:leave-end="opacity-0"
    x-cloak=""
    class="fixed top-16 left-0 right-0 bottom-0 z-40 bg-gray-700 bg-opacity-20"
>
</div>

<div
    x-cloak=""
    x-show="openNav"

    x-transition:enter="transition ease-liner duration-500"
    x-transition:enter-start="transform -translate-x-full"
    x-transition:enter-end="transform translate-x-50"

    x-transition:leave="transition ease-liner duration-500"
    x-transition:leave-start="transform translate-x-50"
    x-transition:leave-end="transform -translate-x-full"

    class="flex flex-col fixed top-16 bottom-0 z-50 w- shadow-xl bg-white overflow-auto"
>
    <div class="flex-grow p-4">

        <nav class="">

            <ul class="space-y-5 text-xl text-blue-500">

                <li class="relative">
                    <a href="#" class="hover:underline">Қазак аттар</a>
                    <div class="absolute top-full mt-0.5 inset-x-0 w-full h-0.5 bg-white rounded-lg"></div>
                </li>
                <li>
                    <a href="#" class="hover:underline">Араб аттары</a>
                </li>
                <li>
                    <a href="#" class="hover:underline">Түркі аттары</a>
                </li>
                <li>
                    <a href="#" class="hover:underline">Аттар жайлы қызық ақпарат</a>
                </li>
                <li>
                    <a href="#" class="hover:underline">Ат қосу</a>
                </li>
            </ul>
        </nav>

    </div>

    <div class="py-2 text-center text-sm">AlatauSystems (c) 2021</div>

</div>